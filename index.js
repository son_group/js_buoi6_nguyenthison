function findNumberPositive(){
    var i = 0;
    var sum = 0;
    while (sum <=10000){
        i++;
        sum +=i;
    }
    document.getElementById("result1").innerHTML = `Số nguyên dương nhỏ nhất có tổng > 10000 là: ${i}`;
    console.log("i: ",i);
}

function calculateSumPow() {
    var xvalue = document.getElementById("txt-x").value*1;
    var nvalue = document.getElementById("txt-n").value*1;
    var sum = 0;
    for(var i=0;i<=nvalue;i++){
        sum += Math.pow(xvalue,i);
        console.log(sum);
    }
    document.getElementById("result2").innerHTML = `Giá trị tổng của số mũ giữa ${xvalue} và ${nvalue} là ${sum}`
    
}

function calculateFactorial(){
    var n = document.getElementById("txt-nvalue").value*1;
    console.log(n);
    var sum=1;
    for(var i= 1;i<=n;i++){
        sum = sum*i;
    }

    document.getElementById("result3").innerHTML = `Giá trị giai thừa của ${n} là ${sum}`;
}


function createDiv(){
    var n = document.getElementById("txt-ndiv").value*1;
    console.log({n});
    var contentHTML = "";
    for(var i=1;i<=n;i++){
        if(i%2==0){
            contentHTML +=`<div class="bg-primary">div chẵn ${i}</div>`
        }else{
            contentHTML +=`<div class="bg-danger">div lẻ ${i}</div>`
        }
    }
    document.getElementById("result4").innerHTML = contentHTML;
}

function findPrimeNumber(n){
    var i = 2;
    while(i<n-1){
        if(n%i==0){
            return false;            
        }
        i++;
    }

    return true;
}

function findNumber(){
    var n = document.getElementById("txt-nnto").value*1;
    var contentHTML ="";
    console.log("n",n);
    for(var i=0;i<=n;i++){
        if(findPrimeNumber(i)){
            contentHTML += `<span class="mr-3">${i}</span>`
            console.log("gia tri", findPrimeNumber(i));
        }
    }
    document.getElementById("result5").innerHTML = contentHTML;
}